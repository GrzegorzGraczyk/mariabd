<html>
   <head>
      <title>Create a MariaDB Database</title>
   </head>

   <body>
      <?php
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
      
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }

         echo 'Connected successfully<br />';
         $sql = 'CREATE DATABASE PRODUCTS';
         $retval = mysqli_query($conn, $sql );
      
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         echo 'Database PRODUCTS created successfully\n';
        
         mysqli_close($conn);
      ?>
   </body>
</html>